<?php

function mini_calculator($first_number, $second_number, $submit) {
    switch ($submit) {
        case "+":
            $sum = $first_number + $second_number;
            return $sum;
            break;
        case "-":
            $sum = $first_number - $second_number;
            return $sum;
            break;
        case "*":
            $sum = $first_number * $second_number;
            return $sum;
            break;
        case "/":
            $sum = $first_number / $second_number;
            return $sum;
            break;
        case "%":
            $sum = $first_number % $second_number;
            return $sum;
            break;
        default:
            echo "I think you are doing something wrong!!";
    }
}
?>

<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Mini Calculator</title>
        <link rel="stylesheet" href="./assets/css/bootstrap.min.css">

        <style>
            input[type=text]{
                width: 50%;
            }
        </style>
    </head>
    <body>
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-md-10">
                        <header><h3 class="text-center">Mini Calculator</h3></header>
                        <hr>
                        <form class="form-horizontal" action="" method="post">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">First Number::</label>
                                <div class="col-sm-10">
                                    <input id="fn" value="<?php if (isset($_POST['first_number'])) echo $_POST['first_number'] ?>" type="text" name="first_number" class="form-control" id="inputEmail3" placeholder="Enter First Number">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword3" class="col-sm-2 control-label">Second Number:: </label>
                                <div class="col-sm-10">
                                    <input id="sn" value="<?php if (isset($_POST['second_number'])) echo $_POST['second_number'] ?>" type="text" name="second_number" class="form-control" id="inputPassword3" placeholder="Enter Second Number">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">

                                    <input class="btn btn-lg" type="submit" value="+" name="submit">
                                    <input class="btn btn-lg" type="submit" value="-" name="submit">
                                    <input class="btn btn-lg" type="submit" value="*" name="submit">
                                    <input class="btn btn-lg" type="submit" value="/" name="submit">
                                    <input class="btn btn-lg" type="submit" value="%" name="submit">

                                </div>
                            </div>
                            <div id="message" class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <h4>
                                        <?php
                                        if (isset($_POST['submit'])) {
                                            $first_number = $_POST['first_number'];
                                            $second_number = $_POST['second_number'];
                                            $submit = $_POST['submit'];
                                            if ($_POST['submit'] == '+') {
                                                echo 'Addition of ' . $first_number . ($_POST['submit']) . $second_number . ' is :: ';
                                            } elseif ($_POST['submit'] == '-') {
                                                echo 'Subtraction of ' . $first_number . ($_POST['submit']) . $second_number . ' is :: ';
                                            } elseif ($_POST['submit'] == '*') {
                                                echo 'Multiplication of ' . $first_number . ($_POST['submit']) . $second_number . ' is :: ';
                                            } elseif ($_POST['submit'] == '/') {
                                                echo 'Division of ' . $first_number . ($_POST['submit']) . $second_number . ' is :: ';
                                            } elseif ($_POST['submit'] == '%') {
                                                echo 'Remainder of ' . $first_number . ($_POST['submit']) . $second_number . ' is :: ';
                                            }
                                            $z = mini_calculator($first_number, $second_number, $submit);
                                            echo $z;
                                        }
                                        ?>
                                    </h4>
                                </div>
                            </div>
                        </form>



                    </div>

                </div>
        </section>

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script type="text/javascript">
            setTimeout(function () {
                $('#message').fadeOut('slow');
                $('#fn').val('');
                $('#sn').val('');
            }, 8000);

        </script>
    </body>
</html>
